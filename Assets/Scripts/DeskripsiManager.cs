﻿using System;

using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeskripsiManager : MonoBehaviour
{
    #region singleton

    public static DeskripsiManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }

    #endregion singleton

    [SerializeField]
    private GameObject deskripsiPanel;

    [SerializeField]
    private Button[] navButtons;

    [SerializeField]
    private Transform[] panels;

    public ar ShowingAR;
    private int indexAR { get => (int)ShowingAR; }

    [Header("Database")]
    [SerializeField]
    private Monumen[] monumenData;

    public Monumen CurrentMonumen;

    [Header("Tampilan")]
    [SerializeField]
    private TextMeshProUGUI judulText;

    [SerializeField]
    private TextMeshProUGUI deskripsiText;

    [SerializeField]
    private Image gMapsImage;

    [SerializeField]
    private Button GmapsButton;

    public void ShowDeskripsi(ar _arToShow)
    {
        ShowingAR = _arToShow;
        CurrentMonumen = monumenData[indexAR];

        applyChanges();
    }

    public void ShowCurrentDeskripsi()
    {
        applyChanges();
    }

    private void applyChanges()
    {
        GmapsButton.onClick.RemoveAllListeners();

        changeDeskripsiText();
        Debug.Log($"showing{ShowingAR}");
        deskripsiPanel.SetActive(true);
    }

    private void changeDeskripsiText()
    {
        Monumen monumen = monumenData[indexAR];

        judulText.text = monumen.nama;
        deskripsiText.text = monumen.deskripsi;
        gMapsImage.sprite = monumen.mapsSpirte;

        GmapsButton.onClick.AddListener(() =>
        {
            Application.OpenURL($"http://maps.google.com/maps?q={judulText.text}");
        });
    }
}

public enum ar
{
    Prambanan, RatuBoko
}