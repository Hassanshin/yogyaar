﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectControl : MonoBehaviour
{
    private Touch touch;
    private Vector2 touchPos;
    private Quaternion rotation;

    private float speed = 0.5f;

    private void Update()
    {
        // If the touch is began
        switch (Input.touchCount)
        {
            case 1:
                // Store one touch.
                touch = Input.GetTouch(0);

                // If the touch moving
                if (touch.phase == TouchPhase.Moved)        // rotate Effect
                {
                    // Get Position change and convert to euler
                    //rotation = Quaternion.Euler(touch.deltaPosition.y * speed, 0, touch.deltaPosition.x * speed);
                    rotation = Quaternion.Euler(touch.deltaPosition.y * speed, -touch.deltaPosition.x * speed, 0);

                    // Apply Rotation
                    rotating(rotation);
                }

                break;

            case 2:                                         // zoom Effect
                // Store two touch.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Get Position change
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Get Position delta
                float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

                // Get Position delta from 2 touch
                float difference = currentMagnitude - prevMagnitude;

                // Apply zooming from 2 touch
                zooming(difference * 0.001f);

                break;

            default:
                break;
        }
    }

    private void zooming(float _scale)
    {
        //delta = _scale;

        float limit = transform.localScale.x;

        limit += _scale;

        limit = Mathf.Clamp(limit, 0.3f, 5f);

        transform.localScale = new Vector3(limit, limit, limit);
    }

    private void rotating(Quaternion _rot)
    {
        transform.rotation = _rot * transform.rotation;
    }

    public void ResetBtn()
    {
        transform.rotation = new Quaternion(0, 0, 0, 0);
        transform.localScale = new Vector3(1, 1, 1);
    }
}