﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectARManager : BaseManager<ObjectARManager>
{
    [SerializeField]
    private GameObject[] modelList;

    [SerializeField]
    private Transform spawnParent;

    private void Start()
    {
        Show(DeskripsiManager.Instance.ShowingAR);
    }

    public void Show(ar _arType)
    {
        modelList[(int)_arType].SetActive(true);
    }
}