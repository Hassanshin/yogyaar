﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : BaseManager<UIManager>
{
    public void Keluar()
    {
        Application.Quit();
    }

    public void StartAR(int _arIndex)
    {
        DeskripsiManager.Instance.ShowingAR = (ar)_arIndex;
        SceneManager.LoadScene(1);
    }

    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ShowDeskripsi(int _arType)
    {
        ar arToShow = (ar)_arType;

        Debug.Log(arToShow);

        DeskripsiManager.Instance.ShowDeskripsi(arToShow);
    }

    public void ShowCurrentDeskripsi()
    {
        DeskripsiManager.Instance.ShowCurrentDeskripsi();
    }
}