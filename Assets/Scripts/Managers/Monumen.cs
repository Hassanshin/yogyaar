﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Monumen", menuName = "Database/MonumenBaru", order = 1)]
public class Monumen : ScriptableObject
{
    public string nama = "Candi ";

    [TextArea(3, 10)]
    public string deskripsi;

    public Sprite mapsSpirte;

    public GameObject modelObject;
}